package com.example.proyectoprueba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends LogActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setDEBUG_TAG(this.getClass().getSimpleName());
        setContentView(R.layout.activity_main);
        setupUI();
    }

    private void setupUI(){
        Button btNextActivity = findViewById(R.id.btNextActivity);
        btNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchNextActivity(view);
            }
        });
    }

    public void launchNextActivity(View view){
        startActivity(new Intent(this,NextActivity.class));
    }

}