package com.example.proyectoprueba;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

public class LogActivity extends AppCompatActivity {
    private static String DEBUG_TAG = " ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        String eventName = "onCreate";
        Log.i(DEBUG_TAG, eventName);
        notify(eventName);
    }

    public void setDEBUG_TAG(String clase) {
        this.DEBUG_TAG = "LOG-"+clase;
    }

    @Override
    protected void onStart() {
        super.onStart();
        String eventName = "onStart";
        Log.i(DEBUG_TAG, eventName);
        notify(eventName);
    }

    @Override
    protected void onStop() {
        super.onStop();
        String eventName = "onStop";
        Log.i(DEBUG_TAG, eventName);
        notify(eventName);
    }

    @Override
    protected void onPause() {
        super.onPause();
        String eventName = "onPause";
        Log.i(DEBUG_TAG, eventName);
        notify(eventName);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String eventName = "onResume";
        Log.i(DEBUG_TAG, eventName);
        notify(eventName);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        String eventName = "onRestart";
        Log.i(DEBUG_TAG, eventName);
        notify(eventName);
        setDEBUG_TAG(this.getClass().getSimpleName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        String whois = (isFinishing())?"USER":"SYSTEM";
        String eventName = "onDestroy";
        Log.i(DEBUG_TAG, eventName+", destroyed by: "+whois);
        notify(eventName+", destroyed by: "+whois);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String eventName = "onRestoreIstanceState";
        Log.i(DEBUG_TAG, eventName);
        notify(eventName);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        String eventName = "onSaveIstanceState";
        Log.i(DEBUG_TAG, eventName);
        notify(eventName);
    }

    private void notify(String eventName){
        String activityName = this.getClass().getSimpleName();

        String CHANNEL_ID = "My_LifeCycle";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,"My Lifecycle",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("lifecycle events");
            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager != null){
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName+" "+activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int)System.currentTimeMillis(),notificationBuilder.build());
    }
}